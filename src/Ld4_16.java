import java.util.Scanner;

public class Ld4_16 {

    private static boolean treeCreated = false;
    private static Tree myTree = null;
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Kristers Lielups 12 171RDB007");

        while (1 < 2){
            printOptions();
            if (sc.hasNextInt()){
                int option = sc.nextInt();
                switch (option){
                    case 1:
                        System.out.println("Ievadiet jaunā koka sakni!");
                        if (sc.hasNextInt()){
                            myTree = new Tree(sc.nextInt());
                            treeCreated = true;
                            break;
                        } else {
                            System.out.println("Vajag ievadīt veselu skaitli!");
                            break;
                        }
                    case 2:
                        if (treeCreated) {
                            System.out.print("Ievadiet skaitli: ");
                            if (sc.hasNextInt()) {
                                myTree.addValue(sc.nextInt());
                            } else {
                                System.out.println("Var pievienot tikai VESELUS SKAITLUS!");
                                sc.next();
                                break;
                            }
                            System.out.print("\n");
                            myTree.display();
                            break;
                        }
                        System.out.println("Nepareizs skaitlis!");
                        break;
                    case 3:
                        if (treeCreated){
                            myTree.display();
                            break;
                        }
                        System.out.println("Nepareizs skaitlis!");
                        break;
                    case 4:
                        if (treeCreated){
                            myTree.withTwoChildren();
                            break;
                        }
                        System.out.println("Nepareizs skaitlis!");
                        break;
                    case 5:
                        if (treeCreated){
                            myTree.negativeNumbers();
                            break;
                        }
                        System.out.println("Nepareizs skaitlis!");
                        break;
                    case 0:
                        System.exit(1);
                        break;
                    default:
                        System.out.println("Nepareizs skaitlis!");
                        break;
                }
            } else {
                System.out.println("Ievadiet derīgu skaitli");
                sc.next();
            }
        }
    }

    private static void printOptions() {
        System.out.println("\n======================================");
        System.out.println(":Izveidot jaunu Koku               : 1");
        if (treeCreated) {
            System.out.println(":Pievienot elementu                 : 2");
            System.out.println(":Izvadīt uz ekrāna (Preorderālais)  : 3");
            System.out.println(":Virsotnes ar 2 bērniem             : 4");
            System.out.println(":Cik negatīvo skaitļu?              : 5");
        }
        System.out.println(":Aizvērt  programmu                 : 0");
        System.out.println("======================================\n");
    }
}

class Tree {

    private static final int MAX_SIZE = 15;
    private Node root;
    private int size;

    Tree(int value) {
        this.root = new Node(value);
        size++;
    }

    void addValue(int value){
        if (size < MAX_SIZE) {
            Node cur = root;
            while (cur != null) {
                if (value <= cur.getValue()) {
                    if (cur.getLeft() != null) {
                        cur = cur.getLeft();
                    } else {
                        cur.setLeft(new Node(value));
                        size++;
                        break;
                    }
                }
                if (value > cur.getValue()) {
                    if (cur.getRight() != null) {
                        cur = cur.getRight();
                    } else {
                        cur.setRight(new Node(value));
                        size++;
                        break;
                    }
                }
            }
        } else {
            System.out.println("Koks ir pilns! Max ir " + MAX_SIZE);
        }
    }

    void withTwoChildren() {
        if (root != null){
            System.out.println("Ir " + root.hasTwoChildren() + " virsotnes ar 2 bērniem!");
        } else {
            System.out.println("Koks ir tukšs");
        }
    }

    void negativeNumbers() {
        if (root != null){
            System.out.println("Ir " + root.countNegativeNumbers() + " negatīvi skaitļi!");
        } else {
            System.out.println("Koks ir tukšs");
        }
    }

    void display() {
        if (root != null){
            root.display();
        } else {
            System.out.println("Koks ir tukšs");
        }
    }
}

class Node {

    private int value;
    private Node left = null, right = null;

    Node(int value) {
        this.value = value;
    }

    void display(){
        System.out.print(value + "\t");
        if (left != null) {
            left.display();
        }
        if (right != null){
            right.display();
        }
    }

    int hasTwoChildren(){
        int counter = 0;
        if (left != null && right != null){
            counter++;
        }
        if (left != null){
            counter += left.hasTwoChildren();
        }
        if (right != null){
            counter += right.hasTwoChildren();
        }
        return counter;
    }

    int countNegativeNumbers(){
        int counter = 0;
        if (value < 0){
            counter++;
        }
        if (left != null){
            counter += left.countNegativeNumbers();
        }
        if (right != null){
            counter += right.countNegativeNumbers();
        }
        return counter;
    }

    int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    Node getLeft() {
        return left;
    }

    void setLeft(Node left) {
        this.left = left;
    }

    Node getRight() {
        return right;
    }

    void setRight(Node right) {
        this.right = right;
    }
}
